package functions

import (
	"os"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func setupSentry() {
	var sentryOptions *sentry.ClientOptions

	if sentryDsn, _ := os.LookupEnv("SENTRY_DSN"); sentryDsn != "" {
		sentryOptions = &sentry.ClientOptions{
			Dsn:         sentryDsn,
			Environment: os.Getenv("STAGE"),
		}
	}

	if sentryOptions != nil {
		// Setting up sentry middleware
		if err := sentry.Init(*sentryOptions); err != nil {
			log.Warn().Err(err).Msg("Sentry Init failed")
			return
		}
		// Adding sentry hook to default Logger
		log.Logger = log.Logger.Hook(sentryHook{})
	} else {
		log.Warn().Msg("Sentry will not be configured")
	}
}

type sentryHook struct{}

func (h sentryHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
	var sl sentry.Level
	switch level {
	case zerolog.ErrorLevel:
		sl = sentry.LevelError
	case zerolog.WarnLevel:
		sl = sentry.LevelWarning
	default:
		return
	}

	event := sentry.NewEvent()
	event.Message = msg
	event.Level = sl
	sentry.CaptureEvent(event)
}
