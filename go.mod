module gitlab.com/thegalabs/go/functions

go 1.16

require (
	github.com/getsentry/sentry-go v0.13.0
	github.com/hashicorp/golang-lru v0.5.4
	github.com/julienschmidt/httprouter v1.3.0
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.1
	gitlab.com/thegalabs/go/errors v0.3.0
)
