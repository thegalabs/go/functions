#!/usr/bin/env bash

function make {
    # shellcheck disable=SC2068,SC2145
    mockery --case underscore --output ./tests/mocks --name=$@
}

make GET --dir router
make POST --dir router
make OPTIONS --dir router
make PUT --dir router
make PATCH --dir router
make DELETE --dir router

make ResponseWriter --srcpkg net/http

make Route --dir router
