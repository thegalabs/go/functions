// Package utils provides common functions
package utils

import (
	"crypto/rand"
	"encoding/base64"
	"math"
)

// RandomString returns a crypto safe random string
func RandomString(length int) (string, error) {
	buff := make([]byte, int(math.Round(float64(length)/float64(1.33333333333)))) //nolint: gomnd
	if _, err := rand.Read(buff); err != nil {
		return "", err
	}
	str := base64.RawURLEncoding.EncodeToString(buff)
	return str[:length], nil
}
