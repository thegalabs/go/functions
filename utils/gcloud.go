package utils

import (
	"io/ioutil"
	"net/http"
)

// GCloudAuthHeader returns the authorization header for a function to function request
// See https://cloud.google.com/functions/docs/securing/authenticating#function-to-function
func GCloudAuthHeader(audience string) (string, error) {
	url := "http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?audience=" + audience
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Metadata-Flavor", "Google")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	token, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return "bearer " + string(token), nil
}
