package functions

import (
	"os"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func setLogLevelFromEnv() {
	level, found := os.LookupEnv("LOG_LEVEL")
	if !found {
		log.Info().Msgf("Current log level is '%s'", zerolog.GlobalLevel().String())
		return
	}

	lInt, err := strconv.Atoi(level)
	if err != nil {
		log.Warn().Msgf("Invalid log level '%s'", level)
		return
	}

	lZero := zerolog.Level(lInt)
	if lZero.String() == "" {
		log.Warn().Msgf("Unkwown log level '%d'", lZero)
		return
	}
	log.Info().Msgf("Set log level '%s'", lZero.String())
	zerolog.SetGlobalLevel(lZero)
}
