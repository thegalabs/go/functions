// Package functions provide a set of useful methods for google cloud functions
package functions

import (
	"github.com/rs/zerolog/log"
)

// Init should be called in the package init
func Init(setup func() error) {
	if err := setup(); err != nil {
		log.Fatal().Err(err).Msg("Could not setup function")
	}

	setupSentry()
	setLogLevelFromEnv()
}
