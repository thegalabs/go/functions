# Functions

A simple utility to make writing cloud functions easier.

## Usage

The main advantages are:
- avoid boilerplate code to write JSON responses
- set up log level using a `LOG_LEVEL` env var
- set up sentry using `SENTRY_DNS` env var
- handle HTTPErrors automatically


```go
var someVariable string

// Setup code
func init() {
    functions.Init(func() (err error) {
        someVariable = "world"
        return nil
    }
}
// Execution
func call(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return nil
    }

    hello := map[string]string {
        "hello": someVariable,
    }

	return functions.JSON(w, 200, hello)
}

// HTTP is the actual cloud function
var HTTP = functions.MakeFunction(errors.DefaultTranslator, call)
```