package router

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

type dummy struct {
	LN              int `json:"ln,omitempty"`
	IP              string
	UA              bool     `json:"ua"`
	SomeList        []string `json:"list"`
	SomeIntList     []int    `json:"ints"`
	OtherStringList []string `json:"strings"`
}

func TestBindForm(t *testing.T) {
	form := url.Values{}
	form.Add("ln", "1")
	form.Add("IP", "Hello")
	form.Add("ua", "true")
	form.Add("list", "a,b,c")
	form.Add("ints", "1,2,3")
	form.Add("strings", "^;^a,b;3")

	var dum dummy
	err := bindForm([]byte(form.Encode()), &dum)

	assert.Nil(t, err)
	assert.Equal(t, 1, dum.LN)
	assert.Equal(t, "Hello", dum.IP)
	assert.True(t, dum.UA)
	assert.Equal(t, []string{"a", "b", "c"}, dum.SomeList)
	assert.Equal(t, []int{1, 2, 3}, dum.SomeIntList)
	assert.Equal(t, []string{"a,b", "3"}, dum.OtherStringList)
}
