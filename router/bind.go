package router

import (
	"errors"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

func fieldName(tag string) string {
	idx := strings.IndexByte(tag, ',')
	if idx == -1 {
		return tag
	}
	return tag[:idx]
}

// this is largely inspired by Echo's implementation
// see https://github.com/labstack/echo/blob/7c8592a7e05370b21cc02cc2edaee6b2355fd83a/bind.go#L120
func bindForm(data []byte, out interface{}) error {
	values, err := url.ParseQuery(string(data))
	if err != nil {
		return ErrInvalidRequest
	}

	return bindValues(values, out)
}

func bindValues(values url.Values, out interface{}) (err error) {
	typ := reflect.TypeOf(out).Elem()
	val := reflect.ValueOf(out).Elem()

	// Map
	if typ.Kind() == reflect.Map {
		for k, v := range values {
			val.SetMapIndex(reflect.ValueOf(k), reflect.ValueOf(v[0]))
		}
		return
	}

	// !struct
	if typ.Kind() != reflect.Struct {
		return errors.New("binding element must be a struct")
	}

	for i := 0; i < typ.NumField(); i++ {
		typeField := typ.Field(i)
		structField := val.Field(i)
		if !structField.CanSet() {
			continue
		}

		fieldKind := structField.Kind()
		tag := typeField.Tag.Get("json")
		switch tag {
		case "-":
			continue
		case "":
			tag = typeField.Name
		default:
			tag = fieldName(tag)
		}

		input := values.Get(tag)
		if input == "" {
			continue
		}

		if err = setWithProperType(fieldKind, input, structField); err != nil {
			return
		}
	}

	return nil
}

// nolint:gocyclo
func setWithProperType(valueKind reflect.Kind, val string, structField reflect.Value) error {
	switch valueKind {
	case reflect.Int:
		return setIntField(val, 0, structField)
	case reflect.Int8:
		return setIntField(val, 8, structField)
	case reflect.Int16:
		return setIntField(val, 16, structField)
	case reflect.Int32:
		return setIntField(val, 32, structField)
	case reflect.Int64:
		return setIntField(val, 64, structField)
	case reflect.Uint:
		return setUintField(val, 0, structField)
	case reflect.Uint8:
		return setUintField(val, 8, structField)
	case reflect.Uint16:
		return setUintField(val, 16, structField)
	case reflect.Uint32:
		return setUintField(val, 32, structField)
	case reflect.Uint64:
		return setUintField(val, 64, structField)
	case reflect.Bool:
		return setBoolField(val, structField)
	case reflect.Float32:
		return setFloatField(val, 32, structField)
	case reflect.Float64:
		return setFloatField(val, 64, structField)
	case reflect.String:
		structField.SetString(val)
	case reflect.Slice:
		return setSlice(val, structField)
	default:
		return errors.New("unknown type")
	}
	return nil
}

func setIntField(value string, bitSize int, field reflect.Value) error {
	intVal, err := strconv.ParseInt(value, 10, bitSize)
	if err == nil {
		field.SetInt(intVal)
	}
	return err
}

func setUintField(value string, bitSize int, field reflect.Value) error {
	uintVal, err := strconv.ParseUint(value, 10, bitSize)
	if err == nil {
		field.SetUint(uintVal)
	}
	return err
}

func setBoolField(value string, field reflect.Value) error {
	boolVal, err := strconv.ParseBool(value)
	if err == nil {
		field.SetBool(boolVal)
	}
	return err
}

func setFloatField(value string, bitSize int, field reflect.Value) error {
	floatVal, err := strconv.ParseFloat(value, bitSize)
	if err == nil {
		field.SetFloat(floatVal)
	}
	return err
}

func setSlice(value string, field reflect.Value) error {
	split := splitListString(value)
	typ := field.Type()
	elem := typ.Elem()
	slice := reflect.MakeSlice(typ, len(split), len(split))

	for i, s := range split {
		if err := setWithProperType(elem.Kind(), s, slice.Index(i)); err != nil {
			return err
		}
	}

	field.Set(slice)
	return nil
}

func splitListString(value string) []string {
	separator := ","
	if value[0] == '^' && value[2] == '^' {
		separator = string(value[1])
		value = value[3:]
	}
	return strings.Split(value, separator)
}
