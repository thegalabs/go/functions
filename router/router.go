// Package router routes requests
package router

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"

	"gitlab.com/thegalabs/go/errors"
)

// Middleware wraps handler calls
type Middleware func(Route) Route

// Route is the actual function call
type Route func(Context) error

// Router wraps an http router
type Router struct {
	// Middlewares that are applied to all routes, before a route's middleware
	Middlewares []Middleware
	// Translator is used to translate error messages returned by the Routes
	Translator errors.Translator

	router *httprouter.Router
}

// New creates a router
func New() *Router {
	return &Router{
		Translator: errors.DefaultTranslator,
		router:     httprouter.New(),
	}
}

// handle creates a handle has understood by httprouter
func (r *Router) handle(handler Route, middlewares []Middleware) httprouter.Handle {
	for _, m := range middlewares {
		handler = m(handler)
	}

	for _, m := range r.Middlewares {
		handler = m(handler)
	}

	return func(rw http.ResponseWriter, request *http.Request, params httprouter.Params) {
		context := NewContext(request, rw, params)

		err := handler(context)
		if err == nil {
			return
		}

		if val := r.Translator(err); val != nil {
			if err = context.JSON(val.GetStatusCode(), val.GetResponse()); err != nil {
				log.Error().Err(err).Msg("Could not write error response")
				rw.WriteHeader(500)
			}
			return
		}

		rw.WriteHeader(500)
		log.Error().Err(err).Msgf("Unknown error type %T", err)
	}
}

// Add registers a route with the router
//
// The global middlewares that will be used are the ones currently set with the Router instance
// Changing them later on will not affect the route
func (r *Router) Add(path string, route interface{}, middlewares ...Middleware) {
	for _, m := range methods(route) {
		r.router.Handle(m.method, path, r.handle(m.handler, middlewares))
	}
}

// Function returns a function that is usable with cloud functions
func (r *Router) Function() func(http.ResponseWriter, *http.Request) {
	return r.router.ServeHTTP
}
