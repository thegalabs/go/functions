package rate_test

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/functions/router"
	"gitlab.com/thegalabs/go/functions/router/middleware/rate"
	"gitlab.com/thegalabs/go/functions/utils"
)

func TestBucketPick(t *testing.T) {
	limit := &rate.BucketLimit{Count: 3, Delay: time.Second}

	bucket := &rate.Bucket{}
	now := time.Now()

	check := func(value bool) {
		assert.Equal(t, value, bucket.Pick(now, *limit))
	}

	for i := 0; i < limit.Count; i++ {
		check(true)
	}
	check(false)

	now = now.Add(limit.Delay + 1)

	for i := 0; i < limit.Count; i++ {
		check(true)
	}
	check(false)
}

func TestPick(t *testing.T) {
	limiter, err := rate.New(
		nil,
		100,
		rate.BucketLimit{Delay: time.Second, Count: 2},
		rate.BucketLimit{Delay: time.Hour, Count: 5},
	)
	assert.Nil(t, err)
	assert.NotNil(t, limiter)

	now := time.Now()

	check := func(value bool) {
		assert.Equal(t, value, limiter.Pick(now, "key"))
	}

	// We can pick 2 before hitting the first limit
	check(true)
	check(true)
	check(false)

	// 2 seconds later we can pick 2 more
	now = now.Add(time.Second * 2)
	check(true)
	check(true)
	check(false)

	// 2 seconds later the second limit kicks in
	now = now.Add(time.Second * 2)
	check(true)
	check(false)

	// Now we are done for a while
	now = now.Add(time.Second * 2)
	check(false)

	// After an hour we are good again
	now = now.Add(time.Hour)
	check(true)
	check(true)
	check(false)
}

func BenchmarkMiddleware(b *testing.B) {
	// Creating random keys
	var keys []string
	for i := 0; i < 150; i++ {
		key, _ := utils.RandomString(20)
		keys = append(keys, key)
	}

	rand.Seed(time.Now().Unix())

	key := func(router.Context) interface{} {
		return keys[rand.Intn(len(keys))] //nolint:gosec
	}

	limiter, _ := rate.New(
		key,
		100,
		rate.BucketLimit{Delay: time.Second, Count: 2},
		rate.BucketLimit{Delay: time.Hour, Count: 5},
	)

	next := func(router.Context) error { return nil }
	route := limiter.Middleware(next)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = route(nil)
	}
}
