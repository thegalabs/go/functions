// Package rate offers a rate limiter middleware
package rate

import (
	"net/http"
	"time"

	// the golang lru cache is not thread safe
	lru "github.com/hashicorp/golang-lru"

	"gitlab.com/thegalabs/go/errors"
	"gitlab.com/thegalabs/go/functions/router"
)

// Bucket holds data about the state of a bucket
type Bucket struct {
	RefillDate      time.Time
	AvailableTokens int
}

// Pick tries to pick a token from the bucket
func (b *Bucket) Pick(now time.Time, limit BucketLimit) bool {
	if now.After(b.RefillDate) {
		b.AvailableTokens = limit.Count
		b.RefillDate = now.Add(limit.Delay)
	}

	b.AvailableTokens--
	// TODO: here we could implement exponential wait times if the Pick method
	// is called when the bucket is empty by increasing the RefillDate
	return b.AvailableTokens >= 0
}

// Key is a way to extract an identifier from a request
type Key func(c router.Context) interface{}

// LimitByIP is a Key that uses the IP address
func LimitByIP(c router.Context) interface{} {
	r := c.Request()
	forwardedFor := r.Header.Get("x-forwarded-for")
	if forwardedFor == "" {
		return r.RemoteAddr
	}
	return forwardedFor
}

// BucketLimit defines how a bucket is filled
type BucketLimit struct {
	Delay time.Duration
	Count int
}

// RateLimiter allows rate limiting requests by IP
//
// The basic algorithm is a bucket filled with tokens:
// - every `Delay` `Count` tokens are added into the bucket
// - every time a request is made it takes a token from the bucket
type RateLimiter struct { //nolint: revive
	cache *lru.Cache

	Limits []BucketLimit

	Key Key
}

// New creates a new ratelimiter
func New(key Key, size int, limits ...BucketLimit) (*RateLimiter, error) {
	cache, err := lru.New(size)
	if err != nil {
		return nil, err
	}

	return &RateLimiter{
		cache:  cache,
		Limits: limits,
		Key:    key,
	}, nil
}

// Pick gets a token from the bucket, refilling it if needed
func (l *RateLimiter) Pick(now time.Time, key interface{}) bool {
	raw, ok := l.cache.Get(key)
	var buckets []Bucket
	if ok {
		buckets = raw.([]Bucket)
	} else {
		buckets = make([]Bucket, len(l.Limits))
		l.cache.Add(key, buckets)
	}
	for i := range buckets {
		if !buckets[i].Pick(now, l.Limits[i]) {
			return false
		}
	}
	return true
}

// Middleware is a router middleware
func (l *RateLimiter) Middleware(next router.Route) router.Route {
	return func(c router.Context) error {
		if !l.Pick(time.Now(), l.Key(c)) {
			return errors.NoResponse(http.StatusTooManyRequests)
		}
		return next(c)
	}
}
