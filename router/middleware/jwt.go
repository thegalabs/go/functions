package middleware

import (
	"encoding/base64"
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"

	"gitlab.com/thegalabs/go/errors"
	"gitlab.com/thegalabs/go/functions/router"
)

// JWTMiddleware parses a JWT and adds all claims to the context
func JWTMiddleware(next router.Route) router.Route {
	return func(c router.Context) error {
		header := c.Request().Header.Get("X-Endpoint-API-UserInfo")
		if header == "" {
			if e := log.Debug(); e.Enabled() {
				if raw, err := json.Marshal(c.Request().Header); err == nil {
					e.RawJSON("headers", raw)
				}
				e.Msg("No endpoint header")
			}

			return errors.NoResponse(http.StatusUnauthorized)
		}

		encoding := base64.URLEncoding
		if len(header)%4 != 0 {
			encoding = base64.RawURLEncoding
		}

		decoded, err := encoding.DecodeString(header)
		if err != nil {
			log.Debug().Msgf("JWT was invalid base64 string with err %s", err)
			return errors.NoResponse(http.StatusUnauthorized)
		}

		var jwt JWT
		if err := json.Unmarshal(decoded, &jwt); err != nil {
			log.Debug().Msgf("JWT was invalid json string with err %s", err)
			return errors.NoResponse(http.StatusUnauthorized)
		}

		c.Set("jwt", jwt)
		c.Set("sub", jwt.Sub)

		return next(c)
	}
}

// JWT is a simplified version of a JWT claim
//
// After benchmarks, it seems that using a hardcoded JWT class was the most efficient compared
// to copying an object or using a map[string]interface{}. If a different JWT class is needed
// one could just duplicate the middleware code I guess.
type JWT struct {
	Sub string `json:"sub"`
	Iss string `json:"iss"`
}
