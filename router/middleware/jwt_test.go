package middleware_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/thegalabs/go/functions/router"
	"gitlab.com/thegalabs/go/functions/router/middleware"
	"gitlab.com/thegalabs/go/functions/tests/mocks"
)

func TestJWTMiddleware(t *testing.T) {
	generator := func(jwt string) func(*testing.T) {
		return func(t *testing.T) {
			route := &mocks.Route{}
			writer := &mocks.ResponseWriter{}

			req, _ := http.NewRequest("", "", nil)
			req.Header.Add("X-Endpoint-API-UserInfo", jwt)

			route.On("Execute", mock.MatchedBy(func(c router.Context) bool {
				assert.Equal(t, "1234567890", c.Get("jwt").(middleware.JWT).Sub)
				return true
			})).Return(nil)

			out := middleware.JWTMiddleware(route.Execute)

			err := out(router.NewContext(req, writer, nil))
			assert.Nil(t, err)
		}
	}

	t.Run("URL", generator("eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJzY29wZSI6WyJoZWxsbyJdfQ"))
	t.Run("Std", generator("eyJ0aWQiOiJhZjYxYjk0My0xZjY4LTRlYWUtOTQ1My1iMjdiMWU0NDEwMzAiLCJpc3MiOiJ1c2VyLnRlY3RlYy5ldSIsImF1ZCI6InRlY3RlYy13ZWIiLCJlbnQiOiJ0ZWN0ZWMiLCJzdWIiOiIxMjM0NTY3ODkwIiwiaWF0IjoxNjIxNDQ0MDcxfQ==")) // nolint: lll
}
