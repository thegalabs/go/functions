// Package middleware has a set of commonly used middlewares
package middleware

import (
	"net/http"

	"gitlab.com/thegalabs/go/errors"

	"gitlab.com/thegalabs/go/functions/router"
)

func getBasicAuthorizationHeader(headers http.Header) string {
	if h := headers.Get("X-Forwarded-Authorization"); len(h) != 0 {
		return h
	}
	return headers.Get("authorization")
}

// BasicMiddleware can be used for basic authentication
// It adds the object returned to the context under the "basic" key
func BasicMiddleware(finder func(string) (interface{}, error)) router.Middleware {
	return func(next router.Route) router.Route {
		return func(c router.Context) error {
			client, err := finder(getBasicAuthorizationHeader(c.Request().Header))
			if err != nil {
				return errors.Code("invalid_request")
			}
			c.Set("basic", client)
			return next(c)
		}
	}
}
