package router

import (
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBindContentType(t *testing.T) {
	generator := func(typ string, success bool) func(*testing.T) {
		return func(t *testing.T) {
			req, err := http.NewRequest("POST", "hello", strings.NewReader(`{"IP": "1"}`))
			require.Nil(t, err)

			req.Header.Add("Content-Type", typ)

			context := NewContext(req, nil, nil)
			var dum dummy
			err = context.Bind(&dum)
			if success {
				assert.Nil(t, err)
				assert.Equal(t, "1", dum.IP)
			} else {
				assert.NotNil(t, err)
			}
		}
	}

	t.Run("JSON", generator("application/json", true))
	t.Run("JSON and", generator("application/json;charset=UTF-8", true))
	t.Run("JSON and and", generator("application/json;charset=UTF-8;whatever", true))

	t.Run("text", generator("text/plain;charset=UTF-8;whatever", false))
}
