package router

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/thegalabs/go/errors"
)

// Context holds data about the request and provides convenience methods for responding
type Context interface {
	Request() *http.Request
	Writer() http.ResponseWriter
	PathParams() httprouter.Params
	Get(string) interface{}
	Set(string, interface{})
	Empty(int) error
	JSON(int, interface{}) error
	Bind(out interface{}, validators ...Validator) error
	BindQuery(out interface{}, validators ...Validator) error
}

type context struct {
	request *http.Request
	writer  http.ResponseWriter

	pathParams httprouter.Params

	arguments map[string]interface{}
}

// NewContext creates a new context
func NewContext(request *http.Request, rw http.ResponseWriter, p httprouter.Params) Context {
	return &context{request, rw, p, make(map[string]interface{})}
}

func (c *context) Request() *http.Request {
	return c.request
}
func (c *context) Writer() http.ResponseWriter {
	return c.writer
}
func (c *context) PathParams() httprouter.Params {
	return c.pathParams
}

// Set adds a value to the context
func (c *context) Set(key string, value interface{}) {
	c.arguments[key] = value
}

// Get returns a value from the context
func (c *context) Get(key string) interface{} {
	return c.arguments[key]
}

// Empty writes an empty response
func (c *context) Empty(status int) error {
	c.writer.WriteHeader(status)
	return nil
}

// JSON writes a json response
func (c *context) JSON(status int, raw interface{}) error {
	c.writer.Header().Add("Content-Type", "application/json")
	c.writer.WriteHeader(status)

	if raw == nil {
		return nil
	}

	b, err := json.Marshal(raw)
	if err != nil {
		return err
	}
	_, err = c.writer.Write(b)
	return err
}

// Validator should return an error if the input is not valid
type Validator func(interface{}) error

// Binder is used to bind a body to a struct, the output must be a pointer
type Binder func([]byte, interface{}) error

// Bind checks for application/json encoding and unmarshals the data
func (c *context) Bind(out interface{}, validators ...Validator) error {
	raw, err := ioutil.ReadAll(c.request.Body)
	defer c.request.Body.Close()
	if err != nil {
		return err
	}

	var binder Binder

	switch strings.SplitN(c.request.Header.Get("content-type"), ";", 2)[0] {
	case "application/json":
		binder = json.Unmarshal
	case "application/x-www-form-urlencoded":
		binder = bindForm
	default:
		return errors.Code("unsupported_encoding")
	}

	if err = binder(raw, out); err != nil {
		return ErrInvalidRequest
	}

	for _, v := range validators {
		if err = v(out); err != nil {
			return err
		}
	}

	return nil
}

func (c *context) BindQuery(out interface{}, validators ...Validator) (err error) {
	if err = bindValues(c.request.URL.Query(), out); err != nil {
		return
	}
	for _, v := range validators {
		if err = v(out); err != nil {
			return
		}
	}
	return
}

// ErrInvalidRequest is returned when the request is not valid
const ErrInvalidRequest = errors.Code("invalid_request")
