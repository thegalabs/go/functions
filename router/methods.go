package router

// OPTIONS handles OPTIONS HTTP Methods
type OPTIONS interface {
	Options(Context) error
}

// GET handles GET HTTP Methods
type GET interface {
	Get(Context) error
}

// POST handles POST HTTP Methods
type POST interface {
	Post(Context) error
}

// PUT handles PUT HTTP Methods
type PUT interface {
	Put(Context) error
}

// PATCH handles PATCH HTTP Methods
type PATCH interface {
	Patch(Context) error
}

// DELETE handles DELETE HTTP Methods
type DELETE interface {
	Delete(Context) error
}

type method struct {
	method  string
	handler Route
}

// methods finds the list of handlers associated with an object
// by casting it to the different interfaces
func methods(route interface{}) (m []method) {
	if r, ok := route.(GET); ok {
		m = append(m, method{"GET", r.Get})
	}
	if r, ok := route.(POST); ok {
		m = append(m, method{"POST", r.Post})
	}
	if r, ok := route.(DELETE); ok {
		m = append(m, method{"DELETE", r.Delete})
	}
	if r, ok := route.(PUT); ok {
		m = append(m, method{"PUT", r.Put})
	}
	if r, ok := route.(PATCH); ok {
		m = append(m, method{"PATCH", r.Patch})
	}
	return
}
